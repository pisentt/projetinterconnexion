#!/usr/bin/env bash
dm_id=$(docker network inspect --format {{.Id}} run_$1)
ip addr show dev dm-${dm_id:0:12}
sudo ip netns add $2
sudo ip link add macvlan_$2 link dm-${dm_id:0:12} type macvlan mode bridge
sudo ip link set macvlan_$2 netns $2 name eth0
sudo ip netns exec $2 ip link set dev lo up
sudo ip netns exec $2 ip link set dev eth0 up
sudo mkdir -p /etc/netns/$2/
sudo touch /etc/netns/$2/resolv.conf
sudo touch /etc/netns/$2/nsswitch.conf
# touch /tmp/resolv.conf_$2
# chmod 777 /tmp/resolv.conf_$2
# sudo ip netns exec $2 unshare -m bash -c "mount --bind /tmp/resolv.conf_$2 /etc/resolv.conf && sudo -u $USER bash -c \"export XDG_RUNTIME_DIR=/run/user/1000 && bash\"" # faire marcher dbus + pipewire etc..
sudo ip netns exec $2 bash -c "export XDG_RUNTIME_DIR=/run/user/1000 && bash" # faire marcher dbus + pipewire etc..
sudo ip netns del $2
echo au revoir
