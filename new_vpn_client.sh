#!/bin/bash

# Set client name
export CLIENTNAME="your_client_name"

# Docker Compose command to generate client certificate without a passphrase (not recommended)
docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME nopass

# Retrieve the client configuration with embedded certificates
docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn

# Fix ownership (adjust as needed)
# sudo chown -R $(whoami): ./openvpn-data
