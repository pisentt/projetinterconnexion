# Projet d'interconnexion 

Téo PISENTI, Nathan AUCHÈRE, Nino GAUTHIER, Gauthier RANCOULE, Hugo SIMON, Mattéo RICARD, Ewen LE BIHAN

## Principaux choix

### Orchestration des différents services

Pour simplifier la tâche et la portabilité du projet, nous avons opté pour une orchestration des différents services via [Docker compose](https://docs.docker.com/compose/). Ce logiciel permet le lancement et la communication inter-container entre différents services, tous encapsulés dans des [Docker](https://docker.com), des VMs légères appelées _containers_.

Nous nous reposons sur les images suivantes : 
- quay.io/frrouting/frr:9.0.1
- [resystit/bind9](https://github.com/resyst-it/docker-bind9)
- [mumblevoip/mumble-server:latest](https://github.com/mumble-voip/mumble-docker)
- [networkboot/dhcpd](https://github.com/networkboot/docker-dhcpd)
- [kylemanna/openvpn](https://github.com/kylemanna/docker-openvpn)
- quay.io/hedgedoc/hedgedoc:1.9.9
    - [postgres:13.4-alpine](https://github.com/docker-library/postgres)
    
 **Nous exposons les réseau d'interconnexion et le réseau privé de la box sur les VLANS 10 et 20 d'un switch de la salle réseau** : cela nous permet de faciliter l'interconnexion avec d'autres groupes qui n'ont pas besoin d'utiliser docker comme nous, mais également de pouvoir tester les service sur des machines réelles de la salle réseau.
 
 On a ainsi connecté plusieurs machines réelles (voir photo plus bas) le jour de l'oral au réseau de la Box via le VLAN 20 du switch qui ont pu récupéré une IP grace au DHCP de la Box et qui ont pu se connecter aux différents services de notre AS.

### Services

Pour notre réseau, nous avons décidé de mettre en place : 
- un service de VOIP : serveur mumble (mumble.net7.dev)
- un service NetBox : logiciel de gestion des équiments réseaux (pas déployé car il pose encore quelques problèmes)
- un service HedgeDoc : logiciel permettant la création collaborative de fichiers markdown (hedgedoc.net7.dev)
- un service DNS : (ns.net7.dev)
- un service DNS dans le réseau d'entreprise (ns.centraverse.fr)
- un service OpenVPN dans le réseau d'entreprise : on peut s'y connecter en utilisant les profils pré-générés "pisentt.ovpn" et "pisento.ovpn"

## Observations

### Difficultés
Nous avons rencontrés quelques difficultés au cours de ce projet, notamment : 
- Faire fonctionner FRRouting
- Comprendre Docker et l'utiliser
    - Lors de la mise en place des box dans les différents réseaux, nous avons notamment été confronté à une particularité de Docker : on ne peut donner à une machine une adresse IP se finissant en .1. Nous avons donc revu notre plan d'adressage en conséquence.
    - Utiliser des volumes avec `docker-compose` et choisir l'emplacement des fichiers (et pouvoir les inculre dans git)
    - VPN : copier fichier config de open-vpn dans l'espace du docker-compose et automatiser les prompts des commandes

## Mise en place de l'architecture

### Plan de l'AS
Notre AS dispose de la plage IP suivante : 120.0.16.0/20
Voici notre découpage en zones : 
![](https://sanik.inpt.fr/uploads/1c49ae9e-63e5-4747-9c66-c6e24fe852e0.png)

Plus complet mais écrit à la main :
![](https://sanik.inpt.fr/uploads/15d46743-4446-4a04-9338-041ca4b17f66.jpg)

Et le plan d'adressage en résultant : 
![](https://sanik.inpt.fr/uploads/e0ee9219-0f36-410b-84bb-7ce4caf48911.png)
On pourra noter la distribution de plages plus ou moins importantes suivant les réseaux (fortement lié à un nombre potentiel plus elevé de machines dans l'un ou l'autre) ainsi que des adresses "distantes" dans le réseau de coeur (afin de mieux ségréguer les services)

### Lancement du projet

0. Installer docker et docker-compose: `sudo pacman -Syu docker docker-compose-plugin` sous Arch, voir [la documentation](https://docs.docker.com/compose/install/linux/#install-using-the-repository) pour les autres distributions
1. Cloner le dépot ou récupérer le dossier
2. Se déplacer dedans
3. Le projet nécéssite la présence d'une **Interface Ethernet "eth0" branchée à un réseau ethernet existant** (pour y attacher les VLAN). Vous pouvez renommer une de vos interfaces réelles avec `sudo ip link set dev <nom interface> name eth0` ou créer une interface virtuelle `sudo ip link add eth0 type dummy`
4. **Assurez-vous que l'interface eth0 est bien up**
5. Lancer `make start`
6. (facultatif) Configurer le switch en mode trunk pour exposer les VLAN 10 et 20 : lire "config_switch.sh"


#### Commandes du `Makefile`

<dl>
<dt><code>make start</code></dt> 
<dd>lancer le projet</dd>

<dt><code>make stop</code></dt>
<dd> arrêter le projet et supprimer les conteneurs/reseaux virtuels </dd>

<dt><code>make reseau machine=&lt;nom du container&gt;</code></dt>
<dd> ouvrir un un shell de l'ordinateur comme si on était dans le réseau d'une machine (on a accès à toutes les commandes de l'ordinateur host) </dd> 

<dt><code>make shell machine=&lt;nom du container&gt;</code></dt>
<dd> ouvrir un shell dans un conteneur (on a accès uniquement aux commandes installées dans le conteneur) </dd>

<dt><code>make add_client client=&lt;nom du client&gt;</code></dt>
<dd> ajouter un client OpenVPN </dd>
</dl>

#### Scripts supplémentaires

<dl>
<dt><code>./create_machine_in_network.sh &lt;nom du reseau&gt; &lt;nom du netns&gt;</code></dt>
<dd> créer un nouveau network namespace dans le réseau du docker spécifié. On a accès à toutes les commandes de l'ordinateur hôte pour réaliser des tests</dd>
    
<dt><code>./create_machine_in_network2.sh test &lt;nom du netns&gt;</code></dt>
<dd> créer un nouveau network namespace dans le réseau eth0.10 (le réseau créé par la box1). On a accès à toutes les commandes de l'ordinateur hôte pour réaliser des tests</dd>
</dl>

## Répartition des tâches

<dl>
    <dt>Téo PISENTI</dt>
    <dd>Chef de groupe, mise en place des routeurs, de l'architecture réseau; écriture du Makefile et scripts pour lancer et tester le projet; debuggage de docker et merge des fonctionnalités pour qu'elles fonctionnent ensemble</dd>
<dt>Nathan AUCHÈRE</dt>
    <dd>Mise en place du DHCP, des routeurs et écriture du rapport</dd>
<dt>Nino GAUTHIER</dt>
    <dd>Mise en place du DHCP
    et mise en place des box</dd>
<dt>Gauthier RANCOULE</dt>
    <dd>Mise en place des routeurs et du VPN</dd>
<dt>Hugo SIMON</dt>
    <dd>Mise en place du service NetBox et du DNS</dd>
<dt>Mattéo RICARD</dt>
    <dd>Mise en place service de VOIP et HedgeDoc (Web et Postgres)</dd>
<dt>Ewen LE BIHAN</dt>
    <dd>Configuration des VLANs, écriture du rapport</dd>
</dl>

<!-- à mettre: -->
<!-- vos principaux choix, vos observations, un
guideline de la mise en place de votre architecture réseau ainsi que la répartition des tâches. -->