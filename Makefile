.PHONY: start stop reseau shell all help dockerd compose_up test_lock

SHELL_HOST=bash
SHELL_CONTENEURS=sh

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

SOURCES=docker-compose.yml $(call rwildcard,$(shell ls -d */ | tr -d / ),*)

SOURCES_COPY=$(patsubst %, .run/%, $(SOURCES))

CHANGER_ROUTES_RULES=$(patsubst %, chg_r_%, dmz_dns pro_dns dmz_mumble serveur_dhcp vpn_entreprise_interne hedgedoc) # netbox netbox-worker netbox-housekeeping)

all: help

help:
	@echo "usage: make start|stop|reseau|shell|add_client"
	@echo ""
	@echo "- start : pas_d'argument          -> lancer le projet"
	@echo "- stop  : pas_d'argument          -> arrêter le projet et supprimer"
	@echo "                                     les conteneurs/reseaux virtuels"
	@echo "- reseau: machine=<nom_conteneur> -> ouvrir un un shell de l'ordinateur comme"
	@echo "                                     si on était dans le réseau d'une machine"
	@echo "                                     (on a accès à toutes les commandes de l'ordinateur host)"
	@echo "- shell : machine=<nom_conteneur> -> ouvrir un shell dans un conteneur"
	@echo "                                     (on a accès uniquement aux commandes installées dans le conteneur)"
	@echo "- add_client: client=<client_name> -> ajouter un client OpenVPN"


# On exécute toutes les commandes sur des copies des fichiers (dans .run)
# de config pour être sûr de ne rien perdre si on modifie les fichiers d'origine
# On utilise .run/lock pour savoir si le projet est déjà lancé pour interdire de restart
start: test_lock compose_up $(CHANGER_ROUTES_RULES)


stop: dockerd
	docker compose -f .run/docker-compose.yml down
	rm .run/lock

vpn_init: 
# docker-compose run --rm openvpn ovpn_genconfig -u udp://120.0.28.10
# docker-compose run --rm openvpn ovpn_initpki nopass
# sudo chown -R gaut: ./openvpn-data
# mv ./openvpn-data .run/openvpn-data
	echo "Permission fixed"
	

reseau:
	sudo nsenter -t $(shell docker inspect --format '{{.State.Pid}}' $(machine)) -n $(SHELL_HOST)

shell:
	docker exec -it $(machine) $(SHELL_CONTENEURS)

add_client: 
	docker exec -it vpn_entreprise_interne easyrsa build-client-full $(client) nopass
	docker exec -it vpn_entreprise_interne ovpn_getclient $(client) > $(client).ovpn

# copier les sources et lancer docker compose up
# (ne doit jamais être appelé par l'user)
compose_up: dockerd $(SOURCES_COPY)
	docker compose -f .run/docker-compose.yml up -d

# fix pour le bug de docker avec frr
# on stoppe le service systemd uniquement si celui-ci était lancé
# et on lance dockerd manuellement
# L'USER NE DOIT PAS LANCER CETTE REGLE -> foire le test du wc -l == 1
dockerd:
ifneq ("$(shell systemctl is-active --quiet containerd && echo Service is running)","")
	sudo systemctl stop docker.service
	sudo systemctl stop docker.socket
	sudo systemctl stop containerd
	sudo sh -c 'dockerd &'
endif
ifeq ("$(shell pgrep -f dockerd | wc -l)","1")
	sudo sh -c 'dockerd &'
endif

# vérifier qu'une session n'a pas déjà été lancée
test_lock: .run
ifneq ("$(wildcard .run/lock)","")
	echo "Le projet a déjà été lancé, utiliser 'make stop' pour supprimer les conteneurs existants."
	exit 1
else
	touch .run/lock
endif

.run:
	mkdir .run

# règles pour copier automatiquement tous les fichiers dans .run
$(SOURCES_COPY): .run/%: %
	mkdir -p $(shell echo $@ | sed "s/\(.*\)\/.*/\1/")
	cp -r $< $@

# changer la route par defaut d'une machine par une qui finit par .2
# (pour fix les routes créées automatiquement par docker compose)
$(CHANGER_ROUTES_RULES): chg_r_%:
	sudo nsenter -t $(shell docker inspect --format '{{.State.Pid}}' $*) -n bash -c "export new_ip=\$$(ip r | grep default | cut -d' ' -f3 | sed 's/\.1$$/\.2/'); ip r del default && ip r add default via \$${new_ip}"
