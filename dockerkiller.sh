#!/bin/bash

containers=$(docker ps -q)

for container in $containers; do
  docker exec -it "$container" kill 1
done
